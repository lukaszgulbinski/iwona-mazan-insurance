﻿using System;
using System.Data.Entity;
using System.Linq;
using Insurance.Domain.Contracts;
using Insurance.Domain.Database;
using System.Collections.Generic;

namespace Insurance.Domain
{
    public class Incidents : IIncidents
    {
        Db db = new Db();

        public Customer CreateCustomer(Guid customerId)
        {
            using (var db = new Db())
            {
                var customer = db.Customers.Find(customerId);
                if (customer == null)
                {
                    customer = new Customer(customerId);
                    db.Customers.Add(customer);
                }
                db.SaveChanges();
                return customer;
            }
        }
        public Customer SaveCustomerDetails(CustomerDetails details)
        {
            Save(details);
            return GetCustomerById(details.CustomerId);
        }
        public Customer SaveCustomerContact(CustomerContact contact)
        {
            Save(contact);
            return GetCustomerById(contact.CustomerId);
        }
        public Customer SaveCustomerAddress(CustomerAddress address)
        {
            Save(address.Value);
            return GetCustomerById(address.CustomerId);
        }

        public Incident CreateIncident(Guid incidentId, Guid customerId)
        {
            using (var db = new Db())
            {
                var customer = db.Customers.Find(customerId);
                if (customer == null) {
                    customer = new Customer(customerId);
                    db.Customers.Add(customer);
                }
                customer.Incidents.Add(new Incident(incidentId, customer));
                db.SaveChanges();
                return GetIncidentById(incidentId);
            }
        }
        public Incident SaveIncidentDetails(IncidentDetails details)
        {
            Save(details);
            return GetIncidentById(details.IncidentId);
        }
        public Incident SaveIncidentDescription(IncidentDescription description)
        {
            Save(description);
            return GetIncidentById(description.IncidentId);
        }
        public Incident SaveIncidentAddress(IncidentAddress address)
        {
            Save(address.Value);
            return GetIncidentById(address.IncidentId);
        }

        public Incident SaveIncidentStatus(IncidentStatus status)
        {
            Save(status);
            return GetIncidentById(status.IncidentId);
        }

        public void SaveAddress(Address address)
        {
            Save(address);
        }
        private void Save<T>(T entity) where T : class
        {
            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
        }

        public IEnumerable<Incident> IncidentsInProcess()
        {
            using (var db = new Db())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var incident = db.Incidents
                    .Include(x => x.Details)
                    .Include(x => x.Description)
                    .Include(x => x.Status)
                    .Include(x => x.Address)
                    .Include(x => x.Address.Value)
                    .Where(x => x.Status.Type == IncidentStatusType.Procesowany)
                    .ToList();
                return incident;
            }
        }

        public Incident GetIncidentById(Guid incidentId)
        {
            using (var db = new Db())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var incident = db.Incidents
                    .Include(x => x.Details)
                    .Include(x => x.Description)
                    .Include(x => x.Status)
                    .Include(x => x.Address)
                    .Include(x => x.Address.Value)
                    .FirstOrDefault(x => x.Id == incidentId);
                return incident;
            }
        }
        public Customer GetCustomerById(Guid customerId)
        {
            using (var db = new Db())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var customer = db.Customers
                    .Include(x => x.Details)
                    .Include(x => x.Address)
                    .Include(x => x.Address.Value)
                    .Include(x => x.Contact)
                    .Include(x => x.Incidents)
                    .Include(x => x.Incidents.Select(y => y.Details))
                    .Include(x => x.Incidents.Select(y => y.Description))
                    .Include(x => x.Incidents.Select(y=> y.Status))
                    .Include(x => x.Incidents.Select(y => y.Address))
                    .Include(x => x.Incidents.Select(y => y.Address.Value))
                    .FirstOrDefault(x => x.Id == customerId);
                return customer;
            }
        }
    }
}
