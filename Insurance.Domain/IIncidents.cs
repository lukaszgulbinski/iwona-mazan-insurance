﻿using Insurance.Domain.Contracts;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Insurance.Domain
{
    [ServiceContract]
    public interface IIncidents
    {
        [OperationContract]
        Customer CreateCustomer(Guid customerId);

        [OperationContract]
        Customer SaveCustomerDetails(CustomerDetails details);

        [OperationContract]
        Customer SaveCustomerContact(CustomerContact contact);

        [OperationContract]
        Customer SaveCustomerAddress(CustomerAddress address);

        
        [OperationContract]
        Incident SaveIncidentDetails(IncidentDetails details);

        [OperationContract]
        Incident SaveIncidentDescription(IncidentDescription description);

        [OperationContract]
        Incident CreateIncident(Guid incidentId, Guid customerId);

        [OperationContract]
        Incident SaveIncidentAddress(IncidentAddress address);

        [OperationContract]
        void SaveAddress(Address address);

        [OperationContract]
        Incident SaveIncidentStatus(IncidentStatus status);

        [OperationContract]
        IEnumerable<Incident> IncidentsInProcess();

        [OperationContract]
        Incident GetIncidentById(Guid incidentId);

        [OperationContract]
        Customer GetCustomerById(Guid customerId);
    }
}