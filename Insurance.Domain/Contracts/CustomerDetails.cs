﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Insurance.Domain.Contracts
{
    public class CustomerDetails
    {
        [DataMember]
        public Guid CustomerId { get; set; }

        [DataMember]
        public string FirstName { get; set; } = string.Empty;

        [DataMember]
        public string LastName { get; set; } = string.Empty;

        [DataMember]
        public string PESEL { get; set; } = string.Empty;

        [IgnoreDataMember]
        public virtual Customer Customer { get; set; }

        public CustomerDetails() { }
        public CustomerDetails(Customer customer)
        {
            if (customer == null) throw new ArgumentNullException("customer");
            CustomerId = customer.Id;
            Customer = customer;
        }
    }
}