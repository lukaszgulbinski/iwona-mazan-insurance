﻿using System;
using System.Runtime.Serialization;

namespace Insurance.Domain.Contracts
{
    [DataContract]
    public class Incident
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public Guid CustomerId { get; set; }
        
        [DataMember]
        public IncidentDetails Details { get; set; }

        [DataMember]
        public IncidentDescription Description { get; set; }

        [DataMember]
        public DateTime CreationDateTime { get; set; }

        [DataMember]
        public IncidentAddress Address { get; set; }

        [DataMember]
        public IncidentStatus Status { get; set; }

        public Incident() { }
        public Incident(Guid incidentId, Customer customer)
        {
            if (incidentId == Guid.Empty) throw new ArgumentException();
            if (customer == null) throw new ArgumentNullException();

            Id = incidentId;
            CustomerId = customer.Id;
            CreationDateTime = DateTime.Now;
            Details = new IncidentDetails(this);
            Description = new IncidentDescription(this);
            Address = new IncidentAddress(this);
            Status = new IncidentStatus(this);
        }
    }
}