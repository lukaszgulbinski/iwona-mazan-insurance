﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using static System.String;

namespace Insurance.Domain.Contracts
{
    public class Address
    {
        [DataMember]
        public Guid Id { get; set; } = Guid.NewGuid();

        [DataMember]
        public string Country { get; set; } = Empty;

        [DataMember]
        public string PostCode { get; set; } = Empty;

        [DataMember]
        public string City { get; set; } = Empty;

        [DataMember]
        public string Street { get; set; } = Empty;

        [DataMember]
        public string StreetNumber { get; set; } = Empty;

        [DataMember]
        public string AppartmentNumber { get; set; } = Empty;

        [IgnoreDataMember]
        public virtual ICollection<IncidentAddress> IncidentAddresses { get; protected set; } = new List<IncidentAddress>();

        [IgnoreDataMember]
        public virtual ICollection<CustomerAddress> CustomerAddresses { get; protected set; } = new List<CustomerAddress>();
    }
}