﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Insurance.Domain.Contracts
{
    [DataContract]
    public class Customer
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public virtual CustomerDetails Details { get; set; }

        [DataMember]
        public virtual CustomerContact Contact { get; set; }

        [DataMember]
        public virtual CustomerAddress Address { get; set; }

        [DataMember]
        public virtual ICollection<Incident> Incidents { get; set; } = new List<Incident>();

        public Customer() { }
        public Customer(Guid id)
        {
            if (id == Guid.Empty) throw new ArgumentException();

            Id = id;
            Details = new CustomerDetails(this);
            Contact = new CustomerContact(this);
            Address = new CustomerAddress(this);
        }
    }
}