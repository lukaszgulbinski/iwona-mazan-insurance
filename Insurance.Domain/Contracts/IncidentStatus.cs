﻿using System;
using System.Runtime.Serialization;

namespace Insurance.Domain.Contracts
{
    public class IncidentStatus
    {
        public IncidentStatus() { }
        public IncidentStatus(Incident incident)
        {
            if (incident == null) throw new ArgumentNullException();
            IncidentId = incident.Id;
            Incident = incident;
            Type = IncidentStatusType.Niewyslane;
            Date = DateTime.MaxValue;
        }

        [DataMember]
        public Guid IncidentId { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public IncidentStatusType Type { get; set; }

        [IgnoreDataMember]
        public virtual Incident Incident { get; set; }
    }

    public enum IncidentStatusType
    {
        Niewyslane,
        Procesowany,
        Odrzucone,
        Zaakceptowane,
    }
}