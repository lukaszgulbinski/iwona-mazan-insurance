﻿using System.Runtime.Serialization;

namespace Insurance.Domain.Contracts
{
    public enum IncidentType
    {
        Niewybrany = 0,
        UbezpieczenieKomunikacyjne = 1,
        UbezpieczenieMajatkowe = 2,
        UbezpieczenieNaZycie = 3
    }
}