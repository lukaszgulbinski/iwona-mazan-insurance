﻿using System;
using System.Runtime.Serialization;

namespace Insurance.Domain.Contracts
{
    public class IncidentDetails
    {
        [DataMember]
        public Guid IncidentId { get; set; }
        
        [IgnoreDataMember]
        public virtual Incident Incident { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string InsuranceNumber { get; set; } = string.Empty;

        [DataMember]
        public IncidentType IncidentType { get; set; }

        public IncidentDetails() { }
        public IncidentDetails(Incident incident)
        {
            if (incident == null) throw new InvalidOperationException();
            IncidentId = incident.Id;
            Incident = incident;
            IncidentType = IncidentType.Niewybrany;
        }
    }
}