﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Insurance.Domain.Contracts
{
    public class CustomerAddress
    {
        [DataMember]
        public Guid CustomerId { get; set; }

        [IgnoreDataMember]
        public virtual Customer Customer { get; set; }

        [IgnoreDataMember]
        public Guid AddressId { get; set; }

        [DataMember]
        public virtual Address Value { get; set; }

        public CustomerAddress() { }
        public CustomerAddress(Customer customer)
        {
            if (customer == null) throw new ArgumentNullException("customer");
            
            CustomerId = customer.Id;
            Customer = customer;
            AddressId = Guid.NewGuid();
            Value = new Address { Id = AddressId };
        }
    }
}