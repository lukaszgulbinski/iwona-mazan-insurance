﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Insurance.Domain.Contracts
{
    public class IncidentAddress
    {
        public IncidentAddress() { }
        public IncidentAddress(Incident incident)
        {
            if (incident == null) throw new ArgumentNullException();
            AddressId = Guid.NewGuid();
            IncidentId = incident.Id;
            Incident = incident;
            Value = new Address { Id = AddressId };
        }

        [DataMember]
        public Guid IncidentId { get; set; }

        [IgnoreDataMember]
        public virtual Incident Incident { get; set; }

        [IgnoreDataMember]
        public Guid AddressId { get; set; }

        [DataMember]
        public virtual Address Value { get; set; }
    }
}