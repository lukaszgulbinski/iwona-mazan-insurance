﻿using System;
using System.Runtime.Serialization;
using static System.String;

namespace Insurance.Domain.Contracts
{
    public class CustomerContact
    {
        [DataMember]
        public Guid CustomerId { get; set; }

        [DataMember]
        public string Phone { get; set; } = Empty;

        [DataMember]
        public string Email { get; set; } = Empty;

        [IgnoreDataMember]
        public virtual Customer Customer { get; set; }

        public CustomerContact() { }
        public CustomerContact(Customer customer)
        {
            if (customer == null) throw new ArgumentNullException(nameof(customer));
            CustomerId = customer.Id;
            Customer = customer;
        }
    }
}