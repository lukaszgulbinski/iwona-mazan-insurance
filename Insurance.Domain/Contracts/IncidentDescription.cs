﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Insurance.Domain.Contracts
{
    public class IncidentDescription
    {
        public IncidentDescription() { }
        public IncidentDescription(Incident incident)
        {
            if (incident == null) throw new ArgumentNullException();
            IncidentId = incident.Id;
            Incident = incident;
        }

        [DataMember]
        public Guid IncidentId { get; set; }

        [IgnoreDataMember]
        public virtual Incident Incident { get; set; }

        [DataMember]
        public string Value { get; set; } = string.Empty;

        [DataMember]
        public DateTime Date { get; set; } = DateTime.MinValue;
    }
}