﻿using Insurance.Domain.Contracts;
using System.Data.Entity.ModelConfiguration;

namespace Insurance.Domain.Database
{
    public class IncidentStatusMap : EntityTypeConfiguration<IncidentStatus>
    {
        public IncidentStatusMap()
        {
            HasKey(x => x.IncidentId);
            Property(x => x.Date).HasColumnType("DateTime2");
        }
    }
}