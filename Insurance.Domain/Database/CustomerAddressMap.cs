﻿using Insurance.Domain.Contracts;
using System.Data.Entity.ModelConfiguration;

namespace Insurance.Domain.Database
{
    class CustomerAddressMap : EntityTypeConfiguration<CustomerAddress>
    {
        public CustomerAddressMap()
        {
            HasKey(x => x.CustomerId);
            HasRequired(x => x.Value).WithMany(x => x.CustomerAddresses).HasForeignKey(x => x.AddressId);
        }
    }
}