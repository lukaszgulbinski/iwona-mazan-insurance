﻿using Insurance.Domain.Contracts;
using System.Data.Entity.ModelConfiguration;

namespace Insurance.Domain.Database
{
    class CustomerContactMap : EntityTypeConfiguration<CustomerContact>
    {
        public CustomerContactMap()
        {
            HasKey(x => x.CustomerId);
            Property(x => x.CustomerId);
            Property(x => x.Email);
            Property(x => x.Phone);
        }
    }
}