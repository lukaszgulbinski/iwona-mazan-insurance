﻿using Insurance.Domain.Contracts;
using System.Data.Entity.ModelConfiguration;

namespace Insurance.Domain.Database
{
    class IncidentAddressMap : EntityTypeConfiguration<IncidentAddress>
    {
        public IncidentAddressMap()
        {
            HasKey(x => x.IncidentId);
            HasRequired(x => x.Value).WithMany(x => x.IncidentAddresses).HasForeignKey(x => x.AddressId);
        }
    }
}