﻿using Insurance.Domain.Contracts;
using System.Data.Entity.ModelConfiguration;

namespace Insurance.Domain.Database
{
    class CustomerMap : EntityTypeConfiguration<Customer>
    {
        public CustomerMap()
        {
            HasKey(x => x.Id);
            HasOptional(x => x.Details).WithRequired(x => x.Customer);
            HasOptional(x => x.Address).WithRequired(x => x.Customer);
            HasOptional(x => x.Contact).WithRequired(x => x.Customer);
            HasMany(x => x.Incidents).WithRequired().HasForeignKey(x => x.CustomerId);
        }
    }
}