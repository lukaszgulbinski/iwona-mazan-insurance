﻿using Insurance.Domain.Contracts;
using System.Data.Entity.ModelConfiguration;

namespace Insurance.Domain.Database
{
    public class CustomerDetailsMap : EntityTypeConfiguration<CustomerDetails>
    {
        public CustomerDetailsMap()
        {
            HasKey(x => x.CustomerId);
        }
    }
}