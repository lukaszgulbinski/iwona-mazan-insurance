﻿using Insurance.Domain.Contracts;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace Insurance.Domain.Database
{
    public class Db : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Incident> Incidents { get; set; }
        public Db() : base("Incidents") {}
        protected override void OnModelCreating(DbModelBuilder db)
        {
            db.Configurations.Add(new CustomerMap());
            db.Configurations.Add(new CustomerDetailsMap());
            db.Configurations.Add(new CustomerContactMap());
            db.Configurations.Add(new CustomerAddressMap());
            
            db.Configurations.Add(new IncidentMap());
            db.Configurations.Add(new IncidentDetailsMap());
            db.Configurations.Add(new IncidentDescriptionMap());
            db.Configurations.Add(new IncidentAddressMap());
            db.Configurations.Add(new IncidentStatusMap());

            db.Configurations.Add(new AddressMap());
            
            base.OnModelCreating(db);
        }
    }
}