﻿using Insurance.Domain.Contracts;
using System.Data.Entity.ModelConfiguration;

namespace Insurance.Domain.Database
{
    class IncidentDetailsMap : EntityTypeConfiguration<IncidentDetails>
    {
        public IncidentDetailsMap()
        {
            HasKey(x => x.IncidentId);
            Property(x => x.IncidentId).IsRequired();
        }
    }
}