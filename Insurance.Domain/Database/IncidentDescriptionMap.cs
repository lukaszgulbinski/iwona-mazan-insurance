﻿using Insurance.Domain.Contracts;
using System.Data.Entity.ModelConfiguration;

namespace Insurance.Domain.Database
{
    class IncidentDescriptionMap : EntityTypeConfiguration<IncidentDescription>
    {
        public IncidentDescriptionMap()
        {
            HasKey(x => x.IncidentId);
            Property(x => x.Date).HasColumnType("DateTime2");
        }
    }
}