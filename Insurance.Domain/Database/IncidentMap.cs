﻿using Insurance.Domain.Contracts;
using System.Data.Entity.ModelConfiguration;

namespace Insurance.Domain.Database
{
    class IncidentMap : EntityTypeConfiguration<Incident>
    {
        public IncidentMap()
        {
            HasKey(x => x.Id);
            HasOptional(x => x.Details).WithRequired(x => x.Incident);
            HasOptional(x => x.Description).WithRequired(x => x.Incident);
            HasOptional(x => x.Address).WithRequired(x => x.Incident);
            HasOptional(x => x.Status).WithRequired(x => x.Incident);
        }
    }
}