﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Security.Principal;

namespace Insurance.Web.Models.Identity
{
    public class User : IUser<Guid>
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public int AccessFailedCount { get; set; }
        public bool LockoutEnabled { get; set; }
        public DateTime LockoutEnd { get; set; }
        public virtual ICollection<Role> Roles { get; set; } = new List<Role>();
    }

    public static class UserHelpers
    {
        public static Guid GetId(this IPrincipal user) => new Guid(user.Identity.GetUserId());
    }
}