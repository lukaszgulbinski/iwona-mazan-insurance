﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;

namespace Insurance.Web.Models.Identity
{
    public class Role : IRole<Guid>
    {
        public static Role Administrator => new Role
        {
            Id = new Guid("3d232832-81cd-43e7-b6f2-de2f250ead34"),
            Name = "administrator"
        };
        public static Role Analyst => new Role
        {
            Id = new Guid("6c1bd487-1d75-48e1-8df9-22253b5d01f8"),
            Name = "analyst"
        };
        public static Role Customer => new Role
        {
            Id = new Guid("76c4a915-a6da-47f4-aac6-d2530d3a6af0"),
            Name = "customer"
        };
        protected Role() { }
        public Guid Id { get; protected set; }
        public string Name { get; set; }
        public virtual ICollection<User> Users { get; set; } = new HashSet<User>();
    }
}