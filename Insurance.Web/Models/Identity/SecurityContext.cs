﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using static Insurance.Web.Models.Identity.Role;
        
namespace Insurance.Web.Models.Identity
{
    public class SecurityContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public SecurityContext() : base("Security")
        {
            Database.SetInitializer(new SecurityInitializationStrategy());
        }
        protected override void OnModelCreating(DbModelBuilder db)
        {
            db.Configurations.Add(new UserMap());
            base.OnModelCreating(db);
        }
    }

    class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            Property(x => x.LockoutEnd).HasColumnType("DateTime2");
            HasMany(x => x.Roles).WithMany(x => x.Users);
        }
    }

    public class SecurityInitializationStrategy : CreateDatabaseIfNotExists<SecurityContext>
    {
        protected override void Seed(SecurityContext db)
        {
            db.Roles.Add(Administrator);
            db.Roles.Add(Analyst);
            db.Roles.Add(Customer);
            db.Users.Add(new User
            {
                Id = new Guid("7527646d-2ece-4367-a1c9-d1c9cf9b7d3b"),
                UserName = "iwonamazandevacc@gmail.com",
                Email = "iwonamazandevacc@gmail.com",
                EmailConfirmed = true,
                LockoutEnabled = false,
                Password = new PasswordHasher().HashPassword("imdevacc"),
                Roles = new List<Role>
                {
                    db.Roles.Find(Administrator.Id),
                    db.Roles.Find(Analyst.Id),
                    db.Roles.Find(Customer.Id)
                }
            });
            base.Seed(db);
        }
    }
}