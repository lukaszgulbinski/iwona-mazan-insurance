﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using static System.String;

namespace Insurance.Web.Models.Identity
{
    public class UserStore :

        IUserStore<User, Guid>,
        IUserEmailStore<User, Guid>,
        IUserPasswordStore<User, Guid>,
        IUserLockoutStore<User, Guid>,
        IUserRoleStore<User, Guid>,
        IUserTwoFactorStore<User, Guid>
    {
        SecurityContext db;

        public UserStore(SecurityContext context)
        {
            this.db = context;
        }

        public async Task AddToRoleAsync(User user, string roleName)
        {
            var role = await db.Roles.FirstAsync(r => r.Name == roleName);
            role.Users.Add(user);
            db.SaveChanges();
        }

        public Task CreateAsync(User user)
        {
            db.Users.Add(user);
            return db.SaveChangesAsync();
        }

        public Task DeleteAsync(User user)
        {
            db.Users.Remove(user);
            return db.SaveChangesAsync();
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public Task<User> FindByEmailAsync(string email)
        {
            return db.Users.FirstOrDefaultAsync(x => x.Email.ToLower() == email.ToLower());
        }

        public Task<User> FindByIdAsync(Guid userId)
        {
            return db.Users.FindAsync(userId);
        }

        public Task<User> FindByNameAsync(string userName)
        {
            return db.Users.FirstOrDefaultAsync(x => x.UserName.ToLower() == userName.ToLower());
        }

        public Task<int> GetAccessFailedCountAsync(User user)
        {
            return Task.FromResult(user.AccessFailedCount);
        }

        public Task<string> GetEmailAsync(User user)
        {
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(User user)
        {
            return Task.FromResult(user.EmailConfirmed);
        }

        public Task<bool> GetLockoutEnabledAsync(User user)
        {
            return Task.FromResult(user.LockoutEnabled);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(User user)
        {
            if (user.LockoutEnd == new DateTime())
                return Task.FromResult(new DateTimeOffset());
            else
                return Task.FromResult(new DateTimeOffset(user.LockoutEnd));
        }

        public Task<string> GetPasswordHashAsync(User user)
        {
            return Task.FromResult(user.Password);
        }

        public Task<IList<string>> GetRolesAsync(User user)
        {
            return Task<IList<string>>.Run(() => (IList<string>)(db.Users.Find(user.Id).Roles.Select(x => x.Name).ToList()));
        }

        public Task<bool> GetTwoFactorEnabledAsync(User user)
        {
            return Task.FromResult(false);
        }

        public Task<bool> HasPasswordAsync(User user)
        {
            return Task.FromResult(user.Password != null);
        }

        public Task<int> IncrementAccessFailedCountAsync(User user)
        {
            user.AccessFailedCount++;
            return Task.FromResult(user.AccessFailedCount);
        }

        public Task<bool> IsInRoleAsync(User user, string roleName)
        {
            return Task.FromResult(db.Users.Find(user.Id).Roles.Any(x => x.Name.ToLower() == roleName.ToLower()));
        }

        public Task RemoveFromRoleAsync(User user, string roleName)
        {
            return Task.Run(() =>
            {
                var userEntity = db.Users.Find(user.Id);
                if (userEntity != null)
                {
                    var role = userEntity.Roles.FirstOrDefault(r => r.Name == roleName);
                    if (role != null)
                    {
                        userEntity.Roles.Remove(role);
                    }
                }
            });
        }

        public Task ResetAccessFailedCountAsync(User user)
        {
            user.AccessFailedCount = 0;
            return Task.FromResult(0);
        }

        public Task SetEmailAsync(User user, string email)
        {
            user.Email = email;
            return Task.FromResult(0);
        }

        public Task SetEmailConfirmedAsync(User user, bool confirmed)
        {
            if (user == null) throw new ArgumentNullException("user");
            return Task.FromResult(user.Email);
        }

        public Task SetLockoutEnabledAsync(User user, bool enabled)
        {
            if (user == null) throw new ArgumentNullException("user");
            user.LockoutEnabled = enabled;
            return Task.FromResult(0);
        }

        public Task SetLockoutEndDateAsync(User user, DateTimeOffset lockoutEnd)
        {
            user.LockoutEnd = lockoutEnd == DateTimeOffset.MinValue ? new DateTime() : lockoutEnd.LocalDateTime;
            return Task.FromResult(0);
        }

        public Task SetPasswordHashAsync(User user, string passwordHash)
        {
            user.Password = passwordHash;
            return Task.FromResult(0);
        }

        public Task SetTwoFactorEnabledAsync(User user, bool enabled)
        {
            throw new NotSupportedException();
        }

        public Task UpdateAsync(User user)
        {
            db.Entry(user).State = EntityState.Modified;
            return db.SaveChangesAsync();
        }
    }

    //public class UserStore :

    //    IUserStore<User, Guid>,
    //    IUserEmailStore<User, Guid>,
    //    IUserPasswordStore<User, Guid>,
    //    IUserLockoutStore<User, Guid>,
    //    IUserRoleStore<User, Guid>,
    //    IUserTwoFactorStore<User, Guid>
    //{
    //    private readonly SecurityClient s = new SecurityClient();
    //    public Task AddToRoleAsync(User user, string roleName) => s.AddToRoleAsync(user, roleName);
    //    public Task CreateAsync(User user) => s.CreateUserAsync(user);
    //    public Task DeleteAsync(User user) => s.DeleteUserAsync(user);
    //    public Task<User> FindByEmailAsync(string email) => s.FindByEmailAsync(email);
    //    public Task<User> FindByIdAsync(Guid userId) => s.FindByIdAsync(userId);
    //    public Task<User> FindByNameAsync(string userName) => s.FindByNameAsync(userName);
    //    public Task<int> GetAccessFailedCountAsync(User user) => s.GetAccessFieldCountAsync(user);
    //    public Task<string> GetEmailAsync(User user) => s.GetEmailAsync(user);
    //    public Task<bool> GetEmailConfirmedAsync(User user) => s.GetEmailConfirmedAsync(user);
    //    public Task<bool> GetLockoutEnabledAsync(User user) => s.GetLockoutEnabledAsync(user);
    //    public Task<DateTimeOffset> GetLockoutEndDateAsync(User user) => s.GetLockoutEndDateAsync(user);
    //    public Task<string> GetPasswordHashAsync(User user) => s.GetPasswordHashAsync(user);
    //    public Task<IList<string>> GetRolesAsync(User user) => Task<IList<string>>.Run(() => (IList<string>)(s.FindById(user.Id).Roles.Select(x => x.Name).ToList()));
    //    public Task<bool> GetTwoFactorEnabledAsync(User user) => s.GetTwoFactorEnabledAsync(user);
    //    public Task<bool> HasPasswordAsync(User user) => s.HasPasswordAsync(user);
    //    public Task<int> IncrementAccessFailedCountAsync(User user) => s.IncrementAccessFailedCountAsync(user);
    //    public Task<bool> IsInRoleAsync(User user, string roleName) => s.IsInRoleAsync(user, roleName);
    //    public Task RemoveFromRoleAsync(User user, string roleName) => s.RemoveFromRoleAsync(user, roleName);
    //    public Task ResetAccessFailedCountAsync(User user) => s.ResetAccessFailedCountAsync(user);
    //    public Task SetEmailAsync(User user, string email) => s.SetEmailAsync(user, email);
    //    public Task SetEmailConfirmedAsync(User user, bool confirmed) => s.SetEmailConfirmedAsync(user, confirmed);
    //    public Task SetLockoutEnabledAsync(User user, bool enabled) => s.SetLockoutEnabledAsync(user, enabled);
    //    public Task SetLockoutEndDateAsync(User user, DateTimeOffset lockoutEnd) => s.SetLockoutEndDateAsync(user, lockoutEnd);
    //    public Task SetPasswordHashAsync(User user, string passwordHash) => s.SetPasswordHashAsync(user, passwordHash);
    //    public Task SetTwoFactorEnabledAsync(User user, bool enabled) => s.SetTwoFactorEnabledAsync(user, enabled);
    //    public Task UpdateAsync(User user) => s.UpdateAsync(user);
    //    public void Dispose() { }
    //}
}