﻿using Microsoft.AspNet.Identity;
using System;
using System.Threading.Tasks;

namespace Insurance.Web.Models.Identity
{
    public class UserManager : UserManager<User, Guid>
    {
        public UserManager(UserStore store) : base(store)
        {
            UserValidator = new UserValidator<User, Guid>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            PasswordValidator = new PasswordValidator
            {
                RequiredLength = 3,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };
            UserLockoutEnabledByDefault = false;
            DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            MaxFailedAccessAttemptsBeforeLockout = 5;
            EmailService = new EmailService();
        }

        public override Task<string> GenerateEmailConfirmationTokenAsync(Guid userId)
        {
            return Task.FromResult(userId.ToString());
        }

        public override async Task<IdentityResult> ConfirmEmailAsync(Guid userId, string token)
        {
            var user = await Store.FindByIdAsync(userId);
            if (user == null)
            {
                return new IdentityResult(new string[] { "Aktywacja konta zakończona niepowodzeniem." });
            }
            user.EmailConfirmed = true;
            await Store.UpdateAsync(user);
            return IdentityResult.Success;
        }
    }
}