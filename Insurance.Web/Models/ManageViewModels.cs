﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace Insurance.Web.Models
{
    public class IndexViewModel
    {
        public bool HasPassword { get; set; }
    }


    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }


    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Aktualne hasło")]
        public string OldPassword { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "{0} musi posiadać conajmniej {2} znaków.", MinimumLength = 3)]
        [DataType(DataType.Password)]
        [Display(Name = "Nowe hasło")]
        public string NewPassword { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "Powtórz nowe hasło")]
        [Compare(nameof(NewPassword), ErrorMessage = "Aktualne oraz nowe hasło muszą być różne.")]
        public string ConfirmPassword { get; set; }
    }
}