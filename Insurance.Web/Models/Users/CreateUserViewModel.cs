﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Insurance.Web.Models.Users
{
    public class CreateUserViewModel
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        [Required]
        [Display(Name = "Hasło")]
        [DataType(DataType.Password)]
        
        public string Password { get; set; } = string.Empty;

        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; } = string.Empty;


        [Display(Name = "Email Potwierdzony")]
        public bool EmailConfirmed { get; set; } = false;


        [Display(Name = "Użytkownik Blokowalny")]
        public bool LockoutEnabled { get; set; } = false;
    }
}