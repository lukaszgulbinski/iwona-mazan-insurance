﻿using System.ComponentModel.DataAnnotations;

namespace Insurance.Web.Models
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }


        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }


        [Display(Name = "Czy zapamiętać hasło w przeglądarce?")]
        public bool RememberMe { get; set; }
    }


    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "{0} musi posiadać co najmniej {2} znaków.", MinimumLength = 3)]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "Powtórz hasło")]
        [Compare(nameof(Password), ErrorMessage = "Powtórzenie hasła nie jest identyczne z wprowadzonym hasłem.")]
        public string ConfirmPassword { get; set; }
    }


    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "{0} musi posiadać co najmniej {2} znaków.", MinimumLength = 3)]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "Powtórz hasło")]
        [Compare(nameof(Password), ErrorMessage = "Powtórzenie hasła nie jest identyczne z wprowadzonym hasłem.")]
        public string ConfirmPassword { get; set; }


        public string Code { get; set; }
    }


    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
