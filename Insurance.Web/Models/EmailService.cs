﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace Insurance.Web.Models
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            SmtpClient smtp = new SmtpClient
            {
                Port = 587,
                Host = "smtp.gmail.com",
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(
                    userName: "iwonamazandevacc@gmail.com",
                    password: "imdevacc")
            };

            return smtp.SendMailAsync(
                from: "iwonamazandevacc@gmail.com",
                recipients: message.Destination,
                subject: message.Subject,
                body: message.Body);
        }
    }
}