﻿using static System.String;
using static Insurance.Web.Incidents.IncidentStatusType;

namespace Insurance.Web.Incidents
{
    public partial class Incident
    {
        public bool IsCompleted =>
            HasDetails &&
            HasDescription &&
            HasAddress;

        public bool HasDetails =>
            Details != null &&
            Details.IsCompleted;

        public bool HasDescription =>
            Description != null &&
            Description.IsCompleted;

        public bool HasAddress =>
            Address != null &&
            Address.IsCompleted;

        public bool CanSubmit =>
            IsCompleted &&
            Status.Type == Niewyslane;

        public bool CanResolve =>
            IsCompleted &&
            Status.Type != Niewyslane;
    }
}