﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static Insurance.Web.Incidents.IncidentType;
using static System.String;

namespace Insurance.Web.Incidents
{
    public class CustomerDetailsMetaData
    {
        [Display(Name = "Imię")]
        [Required(ErrorMessage = "Pole Jest Wymagane")]
        public string FirstName { get; set; }

        [Display(Name = "Nazwisko")]
        [Required(ErrorMessage = "Pole Jest Wymagane")]
        public string LastName { get; set; }
    }

    [MetadataType(typeof(CustomerDetailsMetaData))]
    public partial class CustomerDetails
    {
        public bool IsCompleted =>
            !IsNullOrWhiteSpace(FirstName) &&
            !IsNullOrWhiteSpace(LastName) &&
            !IsNullOrWhiteSpace(PESEL);
    }
}