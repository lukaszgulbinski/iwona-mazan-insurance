﻿using static System.String;

namespace Insurance.Web.Incidents
{
    public partial class CustomerAddress
    {
        public bool IsCompleted =>
            this.Value != null &&
            this.Value.IsCompleted;
    }
}