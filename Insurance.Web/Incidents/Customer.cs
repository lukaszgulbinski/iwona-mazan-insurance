﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Insurance.Web.Incidents
{
    public partial class Customer
    {
        public bool IsCompleted =>
            HasDetails &&
            HasContact &&
            HasAddress;

        public bool HasDetails =>
            Details != null &&
            Details.IsCompleted;

        public bool HasContact =>
            Contact != null &&
            Contact.IsCompleted;

        public bool HasAddress =>
            Address != null &&
            Address.IsCompleted;
    }
}