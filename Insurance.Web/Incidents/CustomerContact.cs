﻿using static System.String;

namespace Insurance.Web.Incidents
{

    public partial class CustomerContact
    {
        public bool IsCompleted =>
            !IsNullOrWhiteSpace(Email) &&
            !IsNullOrWhiteSpace(Phone);
    }
}