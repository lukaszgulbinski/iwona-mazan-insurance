﻿using System.ComponentModel.DataAnnotations;
using static Insurance.Web.Incidents.IncidentType;
using static System.String;

namespace Insurance.Web.Incidents
{
    public class IncidentDetailsMetaData
    {
        [Display(Name = "Nazwa szkody")]
        public string Title { get; set; }

        [Display(Name = "Numer Ubezpieczenia")]
        public string InsuranceNumber { get; set; }

        [Display(Name = "Typ Szkody")]
        public IncidentType IncidentType { get; set; }
    }

    [MetadataType(typeof(IncidentDetailsMetaData))]
    public partial class IncidentDetails
    {
        public bool IsCompleted =>
            !IsNullOrWhiteSpace(Title) &&
            !IsNullOrWhiteSpace(InsuranceNumber) &&
            IncidentType != Niewybrany;
    }
}