﻿using System;
using System.ComponentModel.DataAnnotations;
using static System.String;

namespace Insurance.Web.Incidents
{
    public class IncidentDescriptionMetaData
    {
        [Display(Name = "Opisz Szkody")]
        [Required(ErrorMessage = "Pole Jest Wymagane")]
        public string Value { get; set; }

        [Display(Name = "Data Wystąpienia")]
        [Required(ErrorMessage = "Pole Jest Wymagane")]
        public string Date { get; set; }
    }

    [MetadataType(typeof(IncidentDescriptionMetaData))]
    public partial class IncidentDescription
    {
        public bool IsCompleted =>
            !IsNullOrWhiteSpace(Value) &&
            Date != new DateTime();
    }
}