﻿using System.ComponentModel.DataAnnotations;
using static System.String;

namespace Insurance.Web.Incidents
{
    public class AddressDescriptionMetaData
    {
        [Display(Name = "Państwo")]
        [Required(ErrorMessage = "Pole Jest Wymagane")]
        public string Country { get; set; }

        [Display(Name = "Miescowość")]
        [Required(ErrorMessage = "Pole Jest Wymagane")]
        public string City { get; set; }

        [Display(Name = "Kod Pocztowy")]
        [Required(ErrorMessage = "Pole Jest Wymagane")]
        public string PostCode { get; set; }

        [Display(Name = "Nazwa Ulicy")]
        [Required(ErrorMessage = "Pole Jest Wymagane")]
        public string Street { get; set; }

        [Display(Name = "Numer Ulicy")]
        [Required(ErrorMessage = "Pole Jest Wymagane")]
        public string StreetNumber { get; set; }

        [Display(Name = "Numer Mieszkania")]
        [Required(ErrorMessage = "Pole Jest Wymagane")]
        public string AppartmentNumber { get; set; }
    }

    [MetadataType(typeof(AddressDescriptionMetaData))]
    public partial class Address
    {
        public bool IsCompleted =>
            !IsNullOrWhiteSpace(Country) &&
            !IsNullOrWhiteSpace(City) &&
            !IsNullOrWhiteSpace(PostCode) &&
            !IsNullOrWhiteSpace(Street) &&
            !IsNullOrWhiteSpace(StreetNumber);
    }
}