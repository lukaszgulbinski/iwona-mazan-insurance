﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static System.String;

namespace Insurance.Web.Incidents
{
    public partial class IncidentAddress
    {
        public bool IsCompleted =>
            this.Value != null &&
            this.Value.IsCompleted;
    }
}