﻿using Insurance.Web.Models.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Insurance.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly SecurityContext db;
        public UserController(SecurityContext db)
        {
            this.db = db;
        }

        public ActionResult Index(Guid userId)
        {
            var model = db.Users.Include(x => x.Roles).FirstOrDefault(x => x.Id == userId);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult AddRole(Guid userId)
        {
            if (db.Users.Find(userId) == null)
                return HttpNotFound();

            var model = new AddUserRole
            {
                User = db.Users.Find(userId),
                Roles = db.Roles.ToList(),
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult AddRole(UserRole userRole)
        {
            if (db.Users.Find(userRole.UserId) == null)
                return HttpNotFound();

            if (db.Roles.Find(userRole.RoleId) == null)
                return HttpNotFound();

            db.Users.Find(userRole.UserId).Roles.Add(
                db.Roles.Find(userRole.RoleId));
            db.SaveChanges();

            return RedirectToAction("AddRole", new
            {
                userId = userRole.UserId
            });
        }

        [HttpGet]
        public ActionResult RemoveRole(Guid userId, Guid roleId)
        {
            if (db.Users.Find(userId) == null)
                return HttpNotFound();

            if (db.Roles.Find(roleId) == null)
                return HttpNotFound();

            db.Users.Find(userId).Roles.Remove(db.Roles.Find(roleId));
            db.SaveChanges();

            return RedirectToAction("Index", new
            {
                userId = userId
            });
        }

        [HttpGet]
        public ActionResult Lock(Guid userId)
        {
            var user = db.Users.Find(userId);
            if (user != null && user.LockoutEnabled)
            {
                user.LockoutEnd = DateTime.MaxValue.Date;
                db.SaveChanges();
            }

            return RedirectToAction("Index", new { userId = userId });
        }

        [HttpGet]
        public ActionResult Unlock(Guid userId)
        {
            var user = db.Users.Find(userId);
            if (user != null && user.LockoutEnabled)
            {
                user.LockoutEnd = new DateTime();
                db.SaveChanges();
            }

            return RedirectToAction("Index", new { userId = userId });
        }
    }

    public class AddUserRole
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
        public User User { get; set; }
        public List<Role> Roles { get; set; }
    }

    public class UserRole
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
    }
}