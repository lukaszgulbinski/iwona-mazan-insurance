﻿using System;
using System.Linq;
using System.Web.Mvc;
using Insurance.Web.Incidents;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using Insurance.Web.Models.Identity;

namespace Insurance.Web.Controllers
{
    public class IncidentsController : Controller
    {
        private readonly IncidentsClient incidents;
        public IncidentsController(IncidentsClient service)
        {
            this.incidents = service;
        }

        [HttpGet]
        [Authorize(Roles = "customer")]
        public ActionResult Customer()
        {
            var customer = 
                incidents.GetCustomerById(GetCustomerId()) ??
                incidents.CreateCustomer(GetCustomerId());
            return View("Incidents", customer);
        }

        private Guid GetCustomerId()
        {
            return new Guid(User.Identity.GetUserId());
        }

        [HttpGet]
        [Authorize(Roles = "customer")]
        public ActionResult IncidentEditor(Guid? incidentId)
        {
            if (incidentId == null)
                return RedirectToAction("Customer");

            var customer = incidents.GetCustomerById(GetCustomerId());
            if (!customer.IsCompleted)
            {
                ViewBag.CurrentIncident = incidentId;
                return View("CustomerEditor", customer);
            }

            var incident = customer.Incidents.FirstOrDefault(x => x.Id == incidentId);
            if (incident == null)
            { 
                return RedirectToAction("Customer");
            }

            return View("IncidentEditor", incident);
        }

        [HttpGet]
        [Authorize(Roles = "customer")]
        public ActionResult CreateIncident()
        {
            Guid incidentId = Guid.NewGuid();
            incidents.CreateIncident(incidentId, GetCustomerId());
            return RedirectToAction("IncidentEditor", new { incidentId = incidentId });
        }

        [HttpPost]
        [Authorize(Roles = "customer")]
        public ActionResult IncidentDetailsEditor(IncidentDetails model)
        {
            if (!ModelState.IsValid) return View(model);
            Incident incident = incidents.SaveIncidentDetails(model);
            return View("IncidentEditor", incident);
        }

        [HttpPost]
        [Authorize(Roles = "customer")]
        public ActionResult IncidentDescriptionEditor(IncidentDescription model)
        {
            if (!ModelState.IsValid) return View(model);
            Incident incident = incidents.SaveIncidentDescription(model);
            return View("IncidentEditor", incident);
        }

        [HttpPost]
        [Authorize(Roles = "customer")]
        public ActionResult IncidentAddressEditor(IncidentAddress model)
        {
            if (!ModelState.IsValid) return View(model);
            Incident incident = incidents.SaveIncidentAddress(model);
            return RedirectToAction("IncidentEditor", new { incidentId = incident.Id });
        }

        [HttpPost]
        [Authorize(Roles = "customer")]
        public ActionResult CustomerDetailsEditor(CustomerDetails model, Guid incidentId)
        {
            ViewBag.CurrentIncident = incidentId;
            if (!ModelState.IsValid) return View(model);
            Customer customer = incidents.SaveCustomerDetails(model);
            return View("CustomerEditor", customer);
        }

        [HttpPost]
        [Authorize(Roles = "customer")]
        public ActionResult CustomerContactEditor(CustomerContact model, Guid incidentId)
        {
            ViewBag.CurrentIncident = incidentId;
            if (!ModelState.IsValid) return View(model);
            Customer customer = incidents.SaveCustomerContact(model);
            return View("CustomerEditor", customer);
        }

        [HttpPost]
        [Authorize(Roles = "customer")]
        public ActionResult CustomerAddressEditor(CustomerAddress model, Guid incidentId)
        {
            ViewBag.CurrentIncident = incidentId;
            if (!ModelState.IsValid) return View(model);
            incidents.SaveCustomerAddress(model);
            return RedirectToAction("IncidentEditor", new { incidentId = incidentId });
        }

        [HttpGet]
        [Authorize(Roles = "customer")]
        public ActionResult SubmitIncident(Guid incidentId)
        {
            var incident = incidents.GetIncidentById(incidentId);
            incident.Status.Type = IncidentStatusType.Procesowany;
            incident.Status.Date = DateTime.Now;
            incidents.SaveIncidentStatus(incident.Status);
            return RedirectToAction("Customer");
        }

        [HttpGet]
        [Authorize(Roles = "analyst")]
        public ActionResult IncidentsInProcess()
        {
            IEnumerable<Incident> inProcessList = incidents.IncidentsInProcess();
            return View(inProcessList);
        }

        [HttpGet]
        [Authorize(Roles = "analyst")]
        public ActionResult IncidentBadoo(Guid incidentId)
        {
            var incident = incidents.GetIncidentById(incidentId);
            return View(incident);
        }

        [HttpPost]
        [Authorize(Roles = "analyst")]
        public ActionResult IncidentBadoo(Guid incidentId, IncidentStatusType action)
        {
            var incident = incidents.GetIncidentById(incidentId);
            incident.Status.Type = action;
            incident.Status.Date = DateTime.Now;
            incidents.SaveIncidentStatus(incident.Status);
            return RedirectToAction("IncidentsInProcess");
        }
    }
}