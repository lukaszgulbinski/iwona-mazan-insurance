﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Insurance.Web.Models.Identity;
using System.Threading.Tasks;
using Insurance.Web.Models.Users;
using System.Collections.Generic;

namespace Insurance.Web.Controllers
{
    [Authorize(Roles = "administrator")]
    public class UsersController : Controller
    {
        private SecurityContext db;
        private readonly UserManager userManager;

        public UsersController(
            SecurityContext db,
            UserManager userManager)
        {
            this.db = db;
            this.userManager = userManager;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = db.Users.Include(x => x.Roles).ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateUserViewModel model)
        {
            if (db.Users.Any(x => x.Email == model.Email))
            {
                ModelState.AddModelError("", $@"Użytkownik {model.Email} już istnieje");
            }
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = new User
            {
                Id = model.Id,
                Email = model.Email,
                UserName = model.Email,
                LockoutEnabled = model.LockoutEnabled,
                EmailConfirmed = model.EmailConfirmed,
            };
            var creationResult = await userManager.CreateAsync(user, model.Password);
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (!user.EmailConfirmed)
            {
                string confirmationToken = await userManager
                    .GenerateEmailConfirmationTokenAsync(
                        userId: user.Id);

                var confirmationUrl = Url.Action(
                    actionName: "ConfirmEmailConfirmation",
                    controllerName: "Account",
                    routeValues: new
                    {
                        userId = user.Id,
                        code = confirmationToken
                    },
                    protocol: Request.Url.Scheme);
                await userManager.SendEmailAsync(
                    userId: user.Id,
                    subject: "Aktywuj swoje konto!",
                    body: $@"Użytkownik {User.Identity.Name} utworzył Twoje konto w Iwona Mazan Insurance Group. Prosimy o jego aktywację poprzez kliknięcie w link: {confirmationUrl}.");
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(Guid userId)
        {
            var user = db.Users.Find(userId);
            if (user != null)
            {
                db.Entry(db.Users.Find(userId)).State = EntityState.Deleted;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}
