﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Insurance.Web.Models;
using Insurance.Web.Models.Identity;
using System;

namespace Insurance.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private SignInManager signInManager;
        private UserManager userManager;

        public AccountController(
            UserManager userManager, 
            SignInManager signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }


        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
                return View(model);

            if (userManager.FindByEmail(model.Email)?.EmailConfirmed == false)
                return RedirectToAction(actionName: "ConfirmEmail",controllerName: "Account"); ;

            var signInResult = await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: true);

            switch (signInResult)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Nieudana próba logowania.");
                    return View(model);
            }
        }


        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }


        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User { Id = Guid.NewGuid(), UserName = model.Email, Email = model.Email };
                var userCreationResult = await userManager.CreateAsync(user, model.Password);

                if (userCreationResult.Succeeded)
                {
                    string confirmationToken = await userManager.GenerateEmailConfirmationTokenAsync(user.Id);

                    var confirmationUrl = Url.Action(
                        actionName: "ConfirmEmailConfirmation",
                        controllerName: "Account", 
                        routeValues: new { userId = user.Id, code = confirmationToken }, 
                        protocol: Request.Url.Scheme);

                    await userManager.SendEmailAsync(
                        userId: user.Id,
                        subject: "Aktywuj swoje konto!", 
                        body: $@"Założyłeś konto w Iwona Mazan Insurance Group. Prosimy o jego aktywację poprzez kliknięcie w link: {confirmationUrl}");

                    return RedirectToAction(
                        actionName: "ConfirmEmail",
                        controllerName: "Account");
                }
                AddErrors(userCreationResult);
            }

            // Coś poszło nie tak. Wyświetlamy formularz rejestracji jeszcze raz.
            return View(model);
        }


        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmailConfirmation(Guid userId, string code)
        {
            if (userId == null || code == null)
                return View("Error");

            var result = await userManager.ConfirmEmailAsync(userId, code);
            var identityResult = await userManager.AddToRoleAsync(userId, Role.Customer.Name);
            return View(result.Succeeded && identityResult.Succeeded ? "ConfirmEmailConfirmation" : "Error");
        }


        // GET: /Account/ConfirmEmailRequest
        [AllowAnonymous]
        public ActionResult ConfirmEmail()
        {
            return View("ConfirmEmail");
        }


        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }


        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await userManager.FindByNameAsync(model.Email);
                if (user == null || !(await userManager.IsEmailConfirmedAsync(user.Id)))
                {
                    return View("ForgotPasswordConfirmation");
                }

                string passwordResetToken = await userManager
                    .GeneratePasswordResetTokenAsync(
                        userId: user.Id);

                var callbackUrl = Url.Action(
                    actionName: "ResetPassword", 
                    controllerName: "Account", 
                    routeValues: new {
                        userId = user.Id,
                        code = passwordResetToken }, 
                    protocol: Request.Url.Scheme);

                await userManager
                    .SendEmailAsync(
                        userId: user.Id, 
                        subject: "Zmień hasło!", 
                        body: $@"Resetujesz hasło w serwisie Iwona Mazan Insurance Group. W celu potwierdzenia prosimy o kliknięcie w poniższy link: {callbackUrl}");

                return RedirectToAction(
                    actionName: "ForgotPasswordConfirmation",
                    controllerName: "Account");
            }
            
            return View(model);
        }

        
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await userManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await userManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (userManager != null)
                {
                    userManager.Dispose();
                    userManager = null;
                }

                if (signInManager != null)
                {
                    signInManager.Dispose();
                    signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;
        
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }
            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }
        }
        #endregion
    }
}