﻿using System.Linq.Expressions;

namespace System.Web.Mvc.Html
{
    public static class Helpers
    {
        public static MvcHtmlString FormGroupFor<TModel, TValue>
            (this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            var htmlString = string.Format(
$@"
<div class=""form-group"">
    {html.LabelFor(expression, htmlAttributes: new { @class = "control-label col-md-2" })}
    <div class=""col-md-10"">
        {html.EditorFor(expression, new { htmlAttributes = new { @class = "form-control input-lg" } })}
        {html.ValidationMessageFor(expression, "", new { @class = "text-danger" })}
    </div>
</div>
");
            return new MvcHtmlString(htmlString);
        }

        public static MvcHtmlString FormGroupTextAreaFor<TModel, TValue>
            (this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            var htmlString = string.Format(
$@"
<div class=""form-group"">
    {html.LabelFor(expression, htmlAttributes: new { @class = "control-label col-md-2" })}
    <div class=""col-md-10"">
        {html.TextAreaFor(expression, htmlAttributes: new { @class = "form-control input-lg" })}
        {html.ValidationMessageFor(expression, "", new { @class = "text-danger" })}
    </div>
</div>
");
            return new MvcHtmlString(htmlString);
        }



        public static MvcHtmlString FormEnumDropDownListFor<TModel, TEnum>
            (this HtmlHelper<TModel> html, Expression<Func<TModel, TEnum>> expression)
        {
            var htmlString = string.Format(
$@"
<div class=""form-group"">
    {html.LabelFor(expression, htmlAttributes: new { @class = "control-label col-md-2" })}
    <div class=""col-md-10"">
        {html.EnumDropDownListFor(expression, htmlAttributes: new { @class = "form-control input-lg" })}
        {html.ValidationMessageFor(expression, "", new { @class = "text-danger" })}
    </div>
</div>
");
            return new MvcHtmlString(htmlString);
        }



        public static MvcHtmlString FormGroupDateFor<TModel, TValue>
            (this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            var htmlString = string.Format(
$@"
<div class=""form-group"">
    {html.LabelFor(expression, htmlAttributes: new { @class = "control-label col-md-2" })}
    <div class=""col-md-10"">
        {html.EditorFor(expression, new { htmlAttributes = new { @class = "form-control input-lg", type = "date" } })}
        {html.ValidationMessageFor(expression, "", new { @class = "text-danger" })}
    </div>
</div>
");
            return new MvcHtmlString(htmlString);
        }



        public static MvcHtmlString FormSubmit
            (this HtmlHelper html, string value)
        {
            var htmlString = string.Format(
$@"
<div class=""form-group"">
    <div class=""col-md-offset-2 col-md-10"">
        <input type = ""submit"" value=""{value}"" class=""btn btn-lg btn-default"" />
    </div>
</div>
");
            return new MvcHtmlString(htmlString);
        }



        public static MvcHtmlString FormHiddenFor<TModel, TValue>
            (this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            return html.HiddenFor(expression, new { htmlAttributes = new { @class = "form-control input-lg" } });
        }



        public static MvcHtmlString FormValidationSummary(this HtmlHelper html)
        {
            return html.ValidationSummary(true, "", new { @class = "text-danger" });
        }
      
        
          
        public static MvcHtmlString DataDefinitionFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            var htmlString = string.Format(
$@"
<dt>{html.LabelFor(expression)}</dt>
<dd>{html.DisplayFor(expression)}</dd>
");
            return new MvcHtmlString(htmlString);
        }
    }
}